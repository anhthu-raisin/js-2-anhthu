$(document).ready(function () {
    getProvince()
    $('#province').change(function () {
        getDistrict();
        $('#district').change(function () {
            getWard()
        })
    })
})

function getProvince() {
    $.ajax({
        url: 'https://vapi.vnappmob.com/api/province',
        type: 'GET',
        success: function (data) {
            console.log(data)
            data.results.forEach(element => {
                $('#province').append("<option value ='" + element.province_id + "'>" + element.province_name + "</option>")
            })
        },
        error: function (err) {
            console.log(err)
        },
    })
}

function getDistrict() {
    let province_id = $('#province').val()
    $.ajax({
        url: 'https://vapi.vnappmob.com//api/province/district/' + province_id,
        type: 'GET',
        success: function (data) {
            $('#district').empty();
            data.results.forEach(element => {
                $('#district').append("<option value ='" + element.district_id + "'>" + element.district_name + "</option>")
            })
        },
        error: function (err) {
            console.log(err)
        },
    })
}

function getWard() {
    let district_id = $('#district').val();
    $.ajax({
        url: 'https://vapi.vnappmob.com/api/province/ward/' + district_id,
        type: 'GET',
        success: function (data) {
            $('#ward').empty();
            data.results.forEach(element => {
                $('#ward').append('<option>' + element.ward_name + '</option>')
            })
        },
        error: function (err) {
            console.log(err)
        },
    })
}

